import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Artist;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.artists.GetArtistRequest;
import com.wrapper.spotify.requests.data.artists.GetArtistsTopTracksRequest;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistsTracksRequest;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Main extends Application {
    private SpotifyApi spotifyApi;
    private CountryCode countryCode = CountryCode.SE;
    private ObservableList<Map.Entry<String, String>> words;
    private TreeMap<String, String> trackNames;
    private String path = null;
    private boolean downloading = false;


    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, 500, 500);
        primaryStage.setTitle("Playlist Downloader");
        primaryStage.setScene(scene);
        primaryStage.show();
        String accessToken = ClientCredentialsExample.clientCredentials_Sync();

        spotifyApi = new SpotifyApi.Builder().setAccessToken(accessToken).build();

        HBox hBox = new HBox();
        Label label1 = new Label("Playlist ID:");
        TextField textField = new TextField();
        Button search = new Button("Search");
        Button download = new Button("Download");
        hBox.getChildren().addAll(label1, textField, search, download);
        root.setBottom(hBox);

        search.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> getArtistInfo(textField.getText()));
        download.addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
            if (!downloading) {
                Runnable r = () -> {
                    JFileChooser jfc = new JFileChooser();
                    jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        path = jfc.getSelectedFile().toString();
                        downloadPlaylist();

                    }
                };
                SwingUtilities.invokeLater(r);
                if (path != null) {
                    downloadPlaylist();
                }
            }
        });
        trackNames = new TreeMap<>();
        words = FXCollections.observableArrayList(trackNames.entrySet());
        ListView<Map.Entry<String, String>> listView = new ListView<>(words);
        root.setCenter(listView);


        //GetPlaylistsTracksExample.getPlaylistsTracks_Sync();
        //GetListOfUsersPlaylistsExample.getListOfUsersPlaylists_Sync();
        //AuthorizationCodeUriExample.authorizationCodeUri_Sync();
        //AuthorizationCodeExample.authorizationCode_Sync();
    }

    private void downloadPlaylist() {
        downloading = true;
        for (Map.Entry<String, String> word : words) {
            try {
                Downloader.Downloader(word.getKey(), word.getValue(), path);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
        downloading = false;
    }

    private void getArtistInfo(String id) {
        getFullplaylist(id);
        /*String artistName = getArtist(id);

        Track[] tracks = getArtistsTopTracks(id);


        if (tracks != null) {
            for (Track track : tracks) {
                System.out.println(track.getName());
                trackNames.put(track.getName(), artistName);
            }
        }
        words.addAll(trackNames.entrySet());
        getPlaylist_Sync();*/
    }

    private void getFullplaylist(String playlistId) {
        Paging<PlaylistTrack> playlistInfo = getPlaylistInfo(playlistId);
        int totalNrOfTracksInPlaylist = playlistInfo.getTotal();
        int tracksRemaning = totalNrOfTracksInPlaylist;

        for (int i = 0; i < totalNrOfTracksInPlaylist; i = i + 100) {
            PlaylistTrack[] playlistTracks = getPlaylistsTracks(i, 100, playlistId).getItems();
            if (playlistTracks != null) {
                for (PlaylistTrack playlistTrack : playlistTracks) {
                    System.out.println(playlistTrack.getTrack().getName() + " " + playlistTrack.getTrack().getArtists()[0].getName());
                    trackNames.put(playlistTrack.getTrack().getName(), playlistTrack.getTrack().getArtists()[0].getName());
                }
            }
            tracksRemaning = tracksRemaning - 100;
        }
        words.addAll(trackNames.entrySet());

    }

    String getArtist(String id) {
        GetArtistRequest getArtistRequest = spotifyApi.getArtist(id).build();
        try {
            final Artist artist = getArtistRequest.execute();


            System.out.println("Name: " + artist.getName());
            return artist.getName();
        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    /**
     * @param id of artist
     * @return top 10 tracks of artist
     */
    Track[] getArtistsTopTracks(String id) {
        GetArtistsTopTracksRequest getArtistsTopTracksRequest = spotifyApi.getArtistsTopTracks(id, countryCode).build();
        try {
            final Track[] tracks = getArtistsTopTracksRequest.execute();
            for (Track track : tracks) {
                track.getId();
            }
            System.out.println("Length: " + tracks.length);
            return tracks;


        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }
    }

    Paging<PlaylistTrack> getPlaylistInfo(String playlistId) {
        GetPlaylistsTracksRequest getPlaylistsTracksRequest = spotifyApi
                .getPlaylistsTracks(playlistId)
                .limit(10)
                .offset(0)
                .market(CountryCode.SE)
                .build();

        try {
            //System.out.println("Total: " + playlistTrackPaging.getTotal());
            return getPlaylistsTracksRequest.execute();
        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }

    Paging<PlaylistTrack> getPlaylistsTracks(int offset, int limit, String playlistId) {
        GetPlaylistsTracksRequest getPlaylistsTracksRequest = spotifyApi.getPlaylistsTracks(playlistId)
                .offset(offset)
                .limit(limit)
                .market(CountryCode.SE)
                .build();
        try {
            return getPlaylistsTracksRequest.execute();
        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return null;
    }


    public static void main(String[] args) {
        Application.launch(args);
    }
}

