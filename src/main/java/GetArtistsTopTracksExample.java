import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.artists.GetArtistsTopTracksRequest;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class GetArtistsTopTracksExample {
    private static final String accessToken = "BQA7mizZLtOvQt_S7OKXdp5POQWD0gAvqTMQPdYe_ZGR1ehvZ11kBmm4_Htd1jMxkJNL0g4YSFIyY7Opc-k";
    private static final String id = "6BrvowZBreEkXzJQMpL174";
    private static final CountryCode countryCode = CountryCode.SE;

    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setAccessToken(accessToken)
            .build();
    private static final GetArtistsTopTracksRequest getArtistsTopTracksRequest = spotifyApi
            .getArtistsTopTracks(id, countryCode)
            .build();

    /*public static Track[] getArtistsTopTracks_Sync() {
        //spotifyApi.setAccessToken(accessToken);
        try {

            //final Track[] tracks = getArtistsTopTracksRequest.execute();
            for (Track track : tracks) {
                track.getId();
            }
            System.out.println("Length: " + tracks.length);
            return tracks;


        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
            return null;
        }

    }*/

    public static void getArtistsTopTracks_Async() {
        try {
            final Future<Track[]> artistsFuture = getArtistsTopTracksRequest.executeAsync();

            // ...

            final Track[] tracks = artistsFuture.get();

            System.out.println("Length: " + tracks.length);
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error: " + e.getCause().getMessage());
        }
    }
}