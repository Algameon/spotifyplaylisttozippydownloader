import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import org.apache.commons.io.FileUtils;
import org.controlsfx.control.Notifications;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;

class Downloader {
    static boolean Downloader(String songName, String artist, String path) throws IOException {
        JBrowserDriver downloadDriver = null;
        JBrowserDriver driver = null;
        try {
            String fullQuery = songName + " " + artist + " Extended";
            String query2 = fullQuery.replace(" ", "%20");
            driver = new JBrowserDriver(Settings.builder().ignoreDialogs(true).logWarnings(true).timezone(Timezone.EUROPE_STOCKHOLM).logJavascript(false).logWire(false).loggerLevel(Level.OFF).build());
            driver.get("https://www.zippysharedjs.com/zippyshare/search?q=" + query2);
            String loadedPage = driver.getPageSource();
            //System.out.println(songName);

            Document document = Jsoup.parse(loadedPage);
            //System.out.println(document);
            Element firstSearchResult = document.select("#___gcse_0 > div > div > div > div.gsc-wrapper > div.gsc-resultsbox-visible > div > div > div.gsc-webResult.gsc-result > div.gs-webResult.gs-result > div.gsc-thumbnail-inside > div").first();


            String url = firstSearchResult.selectFirst("a").attr("href");
            String ZippyBaseUrl = url.substring(0, url.indexOf("/v/"));
            //System.out.println(ZippyBaseUrl);

            //System.out.println("maybe an url?" + test.firstElementSibling());
            driver.quit();


            downloadDriver = new JBrowserDriver(Settings.builder().ignoreDialogs(true).logWarnings(true).timezone(Timezone.EUROPE_STOCKHOLM).logJavascript(false).logWire(false).loggerLevel(Level.OFF).build());
            downloadDriver.get(url);
            String downloadPage = downloadDriver.getPageSource();

            Document document1 = Jsoup.parse(downloadPage);
            boolean extended = document1.selectFirst("div.left").toString().toLowerCase().contains("extended");
            //System.out.println(document1.body());
            String downloadLink = document1.select("a#dlbutton").attr("href");

            //System.out.println(downloadlink);
            File fileName;
            if (extended) {
                fileName = new File(path, songName + " (Extended)" + ".mp3");
            } else {
                fileName = new File(path, songName + ".mp3");
            }

            //System.out.println(ZippyBaseUrl + downloadlink);
            URL downloadUrl = new URL(ZippyBaseUrl + downloadLink);
            FileUtils.copyURLToFile(downloadUrl, fileName);
            System.out.println(songName + " - Downloaded");
            downloadDriver.quit();
            return true;

        } catch (Exception e) {
            if (downloadDriver != null) {
                downloadDriver.quit();
            }
            if (driver != null) {
                driver.quit();
            }
            System.out.println(songName + " Not found :(");
            return false;

        }

    }
}