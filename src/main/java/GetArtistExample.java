
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Artist;
import com.wrapper.spotify.requests.data.artists.GetArtistRequest;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class GetArtistExample {
    private static final String accessToken = "BQA7mizZLtOvQt_S7OKXdp5POQWD0gAvqTMQPdYe_ZGR1ehvZ11kBmm4_Htd1jMxkJNL0g4YSFIyY7Opc-k";
    private static final String id = "6BrvowZBreEkXzJQMpL174";

    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setAccessToken(accessToken)
            .build();
    private static final GetArtistRequest getArtistRequest = spotifyApi.getArtist(id)
            .build();

    public static void getArtist_Sync() {
        spotifyApi.setAccessToken(accessToken);
        try {
            final Artist artist = getArtistRequest.execute();

            System.out.println("Name: " + artist.getName());
        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void getArtist_Async() {
        try {
            final Future<Artist> albumFuture = getArtistRequest.executeAsync();

            // ...

            final Artist artist = albumFuture.get();

            System.out.println("Name: " + artist.getName());
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error: " + e.getCause().getMessage());
        }
    }
}