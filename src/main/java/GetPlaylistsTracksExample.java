import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.requests.data.playlists.GetPlaylistsTracksRequest;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class GetPlaylistsTracksExample {
    private static final String accessToken = "BQDWur2yOzRXR_qD9BDcPpQaHS9Stul-2VERxrkuItGe39V22ni1b2NWTCBmPmUMKI4tUs3d8h2ii_MV--U";
    private static final String playlistId = "7iSHabrCcj08RscxOnzvf9";

    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setAccessToken(accessToken)
            .setRefreshToken("AQASfOGGUvylaxQFdxqAVLb_OET-ca5AtUQP2oeVMWlw9b2XUY4xlxB8f4uRpQcJ9dmxOj0mgQjM1ukz-1Gl_pPjjYDspNXNzdsxsptBqH2qF04Wunq06lE_YOq0i4HsyTWKNA")
            .build();
    private static final GetPlaylistsTracksRequest getPlaylistsTracksRequest = spotifyApi
            .getPlaylistsTracks(playlistId)
            .offset(0)
            .market(CountryCode.SE)
            .build();

    public static void getPlaylistsTracks_Sync() {
        try {
            final Paging<PlaylistTrack> playlistTrackPaging = getPlaylistsTracksRequest.execute();
            if (playlistTrackPaging.getItems() != null) {
                for (int i = 0; i < playlistTrackPaging.getItems().length; i++) {
                    System.out.println(playlistTrackPaging.getItems()[i].getTrack().getName());
                }
            }else {
                System.out.println("null :(");
            }
        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void getPlaylistsTracks_Async() {
        try {
            final Future<Paging<PlaylistTrack>> pagingFuture = getPlaylistsTracksRequest.executeAsync();

            // ...

            final Paging<PlaylistTrack> playlistTrackPaging = pagingFuture.get();

            System.out.println("Total: " + playlistTrackPaging.getTotal());
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error: " + e.getCause().getMessage());
        }
    }
}