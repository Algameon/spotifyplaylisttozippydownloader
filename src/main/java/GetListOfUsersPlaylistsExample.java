import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistSimplified;
import com.wrapper.spotify.requests.data.playlists.GetListOfUsersPlaylistsRequest;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class GetListOfUsersPlaylistsExample {
    private static final String accessToken = "BQDo6NG2XHNQk9PvVZhGM1XumZtxlGzb_xCXZIFr8gzpT8F3iQPHbnmkuyVGeHYMqdYIuca4AE5nh_Pfgrc";
    private static final String userId = "algameon";

    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setAccessToken(accessToken)
            .build();
    private static final GetListOfUsersPlaylistsRequest getListOfUsersPlaylistsRequest = spotifyApi
            .getListOfUsersPlaylists(userId)
            .limit(10)
            .offset(0)
            .build();

    public static void getListOfUsersPlaylists_Sync() {
        try {
            final Paging<PlaylistSimplified> playlistSimplifiedPaging = getListOfUsersPlaylistsRequest.execute();

            for (int i = 0; i < playlistSimplifiedPaging.getItems().length; i++) {
                System.out.println(playlistSimplifiedPaging.getItems()[i].getId());
            }
        } catch (IOException | SpotifyWebApiException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void getListOfUsersPlaylists_Async() {
        try {
            final Future<Paging<PlaylistSimplified>> pagingFuture = getListOfUsersPlaylistsRequest.executeAsync();

            // ...

            final Paging<PlaylistSimplified> playlistSimplifiedPaging = pagingFuture.get();

            System.out.println("Total: " + playlistSimplifiedPaging.getTotal());
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error: " + e.getCause().getMessage());
        }
    }
}